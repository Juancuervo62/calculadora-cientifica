/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import static java.lang.Math.*;



/**
 *
 * @author JUAN DAVID CUERVO
 */
public class Operaciontrigo {
    
    double N1;
    double resultado;
    
    double seno(){
        
        resultado=sin(N1);
        return resultado;
    }
    
    double coseno(){
        
        resultado=cos(N1);
        return resultado;
    }
    
    double tangente(){
        
        resultado=tan(N1);
        return resultado;
    }
    
    double cotangente(){
        
        resultado=(1/tan(N1));
        return resultado;
    }
    
    double secante(){
        
        resultado=(1/cos(N1));
        return resultado;
    }
    
    double cosecante(){
        
        resultado=(1/sin(N1));
        return resultado;
    }
}
